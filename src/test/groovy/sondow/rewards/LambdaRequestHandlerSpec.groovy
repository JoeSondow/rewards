package sondow.rewards

import org.junit.Rule
import org.junit.contrib.java.lang.system.EnvironmentVariables

import com.amazonaws.services.lambda.runtime.Context

import spock.lang.Specification
import twitter4j.conf.Configuration

class LambdaRequestHandlerSpec extends Specification {

    @Rule
    public final EnvironmentVariables envVars = new EnvironmentVariables()

    def "handleRequest should create a configured tweeter and tweet an empty string"() {
        setup:
        Context context = Stub()
        Configuration config = Stub()
        PatronCollector tweeter = Mock()
        LambdaRequestHandler handler = Spy()

        when:
        handler.handleRequest(new Object(), context)

        then:
        1 * handler.configure() >> config
        1 * handler.createTweeter(config) >> tweeter
        1 * tweeter.tweet("")
    }


    def "configure should populate a configuration with environment variables"() {
        setup:
        LambdaRequestHandler handler = new LambdaRequestHandler()
        envVars.set("twitter4j_oauth_consumerKey", "scooby")
        envVars.set("twitter4j_oauth_consumerSecret", "shaggy")
        envVars.set("twitter4j_oauth_accessToken", "velma")
        envVars.set("twitter4j_oauth_accessTokenSecret", "daphne")

        when:
        Configuration config = handler.configure()

        then:
        with (config) {
            OAuthConsumerKey == 'scooby'
            OAuthConsumerSecret == 'shaggy'
            OAuthAccessToken == 'velma'
            OAuthAccessTokenSecret == 'daphne'
        }
    }

    def "handleRequest should throw IllegalStateException if environment variables are missing"() {
        setup:
        Context context = Stub()
        LambdaRequestHandler handler = new LambdaRequestHandler()

        when:
        handler.handleRequest(new Object(), context)

        then:
        thrown(IllegalStateException)
    }
}
