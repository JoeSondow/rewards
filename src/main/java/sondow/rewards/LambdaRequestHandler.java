package sondow.rewards;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

/**
 * The function that AWS Lambda will invoke.
 *
 * @author @JoeSondow
 */
public class LambdaRequestHandler implements RequestHandler<Object, Object> {

    /*
     * (non-Javadoc)
     *
     * @see com.amazonaws.services.lambda.runtime.RequestHandler#handleRequest(java. lang.Object,
     * com.amazonaws.services.lambda.runtime.Context)
     */
    @Override
    public Object handleRequest(Object input, Context context) {
//        Configuration config = configure();
//        PatronCollector patronCollector = createTweeter(config);
//        String message = "";
//        return patronCollector.tweet(message);
        return null;
    }

    /**
     * AWS Lambda only allows underscores in environment variables, not dots, so the default ways
     * twitter4j finds keys aren't possible. Instead, write your own code that gets the
     * configuration either from Lambda-friendly environment variables or from a default
     * twitter4j.properties file at the project root, or on the classpath, or in WEB-INF.
     *
     * @return configuration containing Twitter authentication strings
     */
//    /* package */ Configuration configure() {
//        ConfigurationBuilder cb = new ConfigurationBuilder();
//        String consumerKeyEnvVar = Environment.get("twitter4j_oauth_consumerKey");
//        String consumerSecretEnvVar = Environment.get("twitter4j_oauth_consumerSecret");
//        String accessTokenEnvVar = Environment.get("twitter4j_oauth_accessToken");
//        String accessTokenSecretEnvVar = Environment.get("twitter4j_oauth_accessTokenSecret");
//        if (consumerKeyEnvVar != null) {
//            cb.setOAuthConsumerKey(consumerKeyEnvVar);
//        }
//        if (consumerSecretEnvVar != null) {
//            cb.setOAuthConsumerSecret(consumerSecretEnvVar);
//        }
//        if (accessTokenEnvVar != null) {
//            cb.setOAuthAccessToken(accessTokenEnvVar);
//        }
//        if (accessTokenSecretEnvVar != null) {
//            cb.setOAuthAccessTokenSecret(accessTokenSecretEnvVar);
//        }
//        Configuration config = cb.setTrimUserEnabled(true).build();
//        return config;
//    }

    /**
     * Provides a new PatronCollector object.
     *
     * This method is pulled out to make this class easier to unit test with a Spy in Spock.
     *
     * @return the PatronCollector that will do the tweeting
     */
//    /* package */ PatronCollector createTweeter(Configuration config) {
//        return new PatronCollector(config);
//    }
}
