package sondow.rewards;

import com.github.jasminb.jsonapi.JSONAPIDocument;
import com.patreon.PatreonAPI;
import com.patreon.resources.Campaign;
import com.patreon.resources.Pledge;
import com.patreon.resources.User;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Interacts with the Patreon API to get data about patrons such as their twitter handles.
 *
 * @author @JoeSondow
 */
public class PatronCollector {

    public void callPatreon() throws IOException {

        String accessToken = "";  // Get this from the database or patreon's OAuth API or patreon's web site
        // TODO: Store access token, refresh token, and expiration time in database
        // TODO: Automatically refresh access token once per week or at least once per month

        PatreonAPI apiClient = new PatreonAPI(accessToken);
        JSONAPIDocument<User> userResponse = apiClient.fetchUser();
        User user = userResponse.get();
        System.out.println(user.getFullName());
        JSONAPIDocument<List<Campaign>> listJSONAPIDocument = apiClient.fetchCampaigns();
        List<Campaign> campaigns = listJSONAPIDocument.get();
        if (campaigns != null && campaigns.size() > 0) {
            for (Campaign c : campaigns) {
                System.out.println("campaign creation name: " + c.getCreationName() +
                        ", creator: " + c.getCreator().getFullName() +
                        ", one liner: " + c.getOneLiner() + ", patron count: " + c.getPatronCount() +
                        ",pledge sum: " + c.getPledgeSum() + ", campaign id: " + c.getId());
                JSONAPIDocument<List<Pledge>> pledgesDoc = apiClient.fetchPageOfPledges(c.getId(), 300,
                        null, Arrays.asList(Pledge.PledgeField.AmountCents, Pledge.PledgeField.DeclinedSince,
                                Pledge.PledgeField.IsPaused, Pledge.PledgeField.PatronPaysFees));
                // TODO: get multiple pages of pledges

                List<Pledge> pledges = pledgesDoc.get();
                List<Pledge> goodStanding = new ArrayList<>();
                for (Pledge pledge : pledges) {
                    if (!pledge.getPaused() && pledge.getDeclinedSince() == null) {
                        goodStanding.add(pledge);

                    }
                }
                goodStanding.sort(Comparator.comparing(Pledge::getAmountCents).reversed());
                System.out.println("Number of pledges in good standing: " + goodStanding.size());
                List<Pledge> withTwitter = new ArrayList<>();
                for (Pledge pledge : goodStanding) {

                    User patron = pledge.getPatron();
                    System.out.println(
                            "amount cents: " + pledge.getAmountCents() +
                                    ", full name: " + patron.getFullName() +
                                    ", email: " + patron.getEmail() +
                                    ", twitter: " + patron.getTwitter() +
                                    (pledge.getPatronPaysFees() ? ", patron pays fees: true" : ""));
                    if (patron.getTwitter() != null && patron.getTwitter().length() >= 1) {
                        withTwitter.add(pledge);
                    }
                }
                System.out.println("Number of pledges with twitter: " + withTwitter.size());
                for (Pledge pledge : withTwitter) {
                    User patron = pledge.getPatron();
                    System.out.println(
                            "amount cents: " + pledge.getAmountCents() +
                                    ", full name: " + patron.getFullName() +
                                    ", email: " + patron.getEmail() +
                                    ", twitter: " + patron.getTwitter() +
                                    (pledge.getPatronPaysFees() ? ", patron pays fees: true" : ""));
                }

            }
        }
        // You should save the user's PatreonOAuth.TokensResponse in your database
        // (for refreshing their Patreon data whenever you like),
        // along with any relevant user info or pledge info you want to store.
    }

    public static void main(String[] args) throws IOException {
        new PatronCollector().callPatreon();
    }
}
